FROM node:latest

WORKDIR /Fullstack/mern/backend

COPY package.json .

RUN npm install

COPY . .

EXPOSE 5000

CMD ["npm", "run", "server"]

const express = require("express");
const dotenv = require("dotenv").config();
const port = process.env.PORT;
const colors = require("colors");
const dbConnection = require("./config/db");
const { errorHandeler } = require("./middleware/errorMiddleware");

dbConnection();

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/api/goals", require("./routes/goalRoutes"));

app.use("/api/users", require("./routes/userRoutes"));

app.use("/api/test", require("./routes/testRoutes"));
app.use(errorHandeler);

app.listen(port, () => console.log(`Server running on ${port}`));

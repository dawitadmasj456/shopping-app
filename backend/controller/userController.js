const asyncHandler = require('express-async-handler')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../model/userModel')
//@desc registering a user
//@access public
//@method Post
const registerUser = asyncHandler( async (req,res) => {
     const {name, email, password} = req.body
    
     if(!name || !email || !password){
        
        res.status(400)
        throw new Error('Please add your fields')
      
     }
     //check a user exists

     const userExists = await User.findOne({email})
     
     if(userExists){

        res.status(400)
        throw new Error('there is an user at this email')
     }
     //hash the password

     const salt = await bcrypt.genSalt(10)
     const hashedPassword = await bcrypt.hash(password, salt)
     //

     const user = await User.create({
        name,
        email,
        password:hashedPassword

     })
     if(user){
        console.log("add your field")
        res.status(201).json({
            _id:user.id,
            name,
            email,
            token: generateToken(user._id)
        })
    }
    else{
        res.status(400)
        throw new Error('user not registered')
    }

})
//@desc logging a user
//@access public
//method post /api/login
const loginUser =  asyncHandler( async (req,res) => {
    const {email, password}= req.body

    //check the fields are not empty
    if(!email|| !password){
        res.status(400)
        throw new Error('Please add the fields')
    }

   
    // check wheather the user data is the same to the database data

    const user = await User.findOne({email})
    if(user && (await bcrypt.compare(password, user.password))){
        res.status(201).json({
            _id:user.id,
            name: user.name,
            email,
            token: generateToken(user._id)
        })
        
    }
    else{
        res.status(400)
        throw new Error('invalid user ')
    }

})
//@desc feaching a user data
//@access public
//method Get
const getme =  asyncHandler( async (req,res) => {

    const {_id,name,email}= await User.findById(req.user.id)

    res.json({
        id:_id,
        name,
        email,
    })

})

const generateToken = (id) => {
    return jwt.sign({id}, process.env.JWT_SECRET,{
        expiresIn:'30d'
    })
}
module.exports = {
    registerUser,
    loginUser,
    getme
}
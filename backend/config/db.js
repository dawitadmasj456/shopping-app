const mongoose = require('mongoose')

const dbConnection = async () => {

    try {
        const conn = await mongoose.connect(process.env.MONGODB_URI)

        console.log(`Mongo db is connected ${conn.connection.host}`.cyan.underline)
    } catch (error) {
        console.log(error)
        process.exit(1)
    }
}

module.exports = dbConnection